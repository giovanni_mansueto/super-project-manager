Super Project Manager
=====================================

### Getting repository

1. Clone this repo from `https://bitbucket.org/giovanni_mansueto/super-project-manager`

### Getting up and running server application

1. Run `npm install` from the `server` directory
2. Run `npm start`  from the `server` directory
3. Now, our server is running on port 5000

For now, use this same steps to production.

### Getting up and running client application

1. Open another bash prompt 
2. Run `npm install` from the root directory
3. Run `npm run dev` from the root directory
4. Your browser will automatically be opened and directed to the browser-sync proxy address
5. To prepare assets for production, run the `npm run build` script (Note: the production task does not fire up the express server, and won't provide you with browser-sync's live reloading. Simply use `npm run dev` during development. More information below)

Now that `npm run dev` is running, the server is up as well and serving files from the `/build` directory. Any changes in the `/app` directory will be automatically processed by Gulp and the changes will be injected to any open browsers pointed at the proxy address.

### Usage
##### Signin data:

Username: `giovanni` Password: `123`

Or feel free to register a new account.

