function OnRun($rootScope, $state, AppSettings, ProjectService, AuthService) {
	'ngInject';

	$rootScope.isPageLoaded = false;

	$rootScope.projects = [];

	$rootScope.bodyClass = '';
	
	$rootScope.currentUser = null;

	function fetchData(userId) {
		return ProjectService
			.getAll(userId)
			.then((r) => {
				if (!r) {
					return;
				}

				$rootScope.projects = r;

				$rootScope.$safeApply();

				return r;

			});
	}

	function evaluateState() {
		if ($rootScope.projects && !$rootScope.projects.length) {
			$rootScope.goToState('new-project');
		} else {
			$rootScope.goToState('project', {
				id: $rootScope.projects[0].id
			});
		}
	}

	function init() {
		$rootScope.currentUser = AuthService.getCurrent();

		function broadcastAppLoaded() {
			$rootScope.$broadcast('app.data.loaded', true);
		}

		if($rootScope.currentUser) {
			fetchData($rootScope.currentUser.id)
				.then(() => {
					broadcastAppLoaded();
				});
		}else {
			broadcastAppLoaded();
		}
	}

	// Subscribers
	$rootScope.$on('$stateChangeSuccess', (event, toState) => {
		$rootScope.pageTitle = '';

		if (toState.title) {
			$rootScope.pageTitle += toState.title + ' - ';
		}

		$rootScope.bodyClass = toState.bodyClass ? toState.bodyClass : '';

		$rootScope.pageTitle += AppSettings.appTitle;
	});

	$rootScope.$on('app.data.loaded', (event, isLoaded) => {
		$rootScope.isPageLoaded = isLoaded;		
		$rootScope.$safeApply();
	});

	$rootScope.$on('app.user.authenticated', (event, user) => {
		if(typeof user === 'object') {
			$rootScope.currentUser = user;
			fetchData($rootScope.currentUser.id);
		}
	});

	$rootScope.$on('app.user.unauthenticated', () => {
		$rootScope.currentUser = null;
		$rootScope.projects = [];
	});

	$rootScope.$on('app.project.updated', () => {
		fetchData($rootScope.currentUser.id);
	});

	$rootScope.$on('app.project.deleted', () => {
		fetchData($rootScope.currentUser.id)
			.then(() => {
				evaluateState();
			});
	});

	//Methods
	$rootScope.$safeApply = function(fn) {
		const phase = this.$root.$$phase;

		if (phase === '$apply' || phase === '$digest') {
			if (fn && (typeof fn === 'function')) {
				fn();
			}
		} else {
			this.$apply(fn);
		}
	};

	$rootScope.goToState = function(state, params, options) {
		params = params || {};
		options = options || {};

		$state.go(state, params, options);
	};

	init();

}

export default OnRun;
