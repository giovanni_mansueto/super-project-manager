function Trim() {

	return (value) => {
		if (!angular.isString(value)) {
			return '';
		}
		return value.replace(/^\s+|\s+$/g, '');
	};

}

export default {
	name: 'trim',
	fn: Trim
};
