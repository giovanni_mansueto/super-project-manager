import angular from 'angular';

// angular modules
import constants from './constants';
import onConfig from './on_config';
import onRun from './on_run';

import 'datejs';
import 'angular-local-storage';
import 'angular-ui-router';
import 'angular-ui-bootstrap';
import 'sweetalert';
import 'ng-sweet-alert';
import './templates';
import './filters';
import './controllers';
import './services';
import './directives';

// create and bootstrap application
const requires = [
	'ui.router',
	'ui.bootstrap',
	'ng-sweet-alert',
	'LocalStorageModule',
	'templates',
	'app.filters',
	'app.controllers',
	'app.services',
	'app.directives'
];

// mount on window for testing
window.app = angular.module('app', requires);

app
	.constant('AppSettings', constants)
	.config(onConfig)
	.run(onRun);

angular.bootstrap(document, ['app'], {
	strictDi: true
});
