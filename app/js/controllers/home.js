function HomeCtrl($scope, $rootScope) {
	'ngInject';

	function evaluateState() {
		if(!$rootScope.currentUser) {
			$rootScope.goToState('signin');
		}

		if(!$rootScope.isPageLoaded) {
			return;
		}

		if ($rootScope.projects && !$rootScope.projects.length) {
			$rootScope.goToState('new-project');
		} else {
			$rootScope.goToState('project', {
				id: $rootScope.projects[0].id
			});
		}

		$rootScope.$safeApply();

	}

	let subscriber = $rootScope.$on('app.data.loaded', evaluateState);

	// Destructor
	$scope.$on('$destroy', () => {
	  // Remove subscriber
	  subscriber();
	  
	});

	evaluateState();

}

export default {
	name: 'HomeCtrl',
	fn: HomeCtrl
};
