function ProjectCtrl($scope, $rootScope, projectId, SweetAlert, ProjectService, TaskService) {
	'ngInject';

	// ViewModel
	const vm = this;

	let subscribers = {};

	vm.loading = true;
	vm.selectedFilter = 'all';
	vm.isNew = !projectId;
	vm.project = {};
	vm.tasks = [];

	function deleteProject() {

    function deleteAction() {
      if (vm.isNew) {
        return;
      }

      vm.loading = true;
      ProjectService
        .delete(projectId)
        .then(() => {
        	vm.loading = false;
        	$rootScope.$safeApply();
        });
    }

		SweetAlert.confirm('Are you sure?')
			.then((p) => {
				if (p) {
					deleteAction();
				}
			});
	}

	function fetchData(id) {
		function onError() {
			$rootScope.goToState('new-project');
		}

		ProjectService
			.get($rootScope.currentUser.id, id)
			.then((r) => {
				vm.loading = false;

				if (!r) {
					return;
				}

				vm.project = r;

				TaskService
					.getAllByProject(projectId, true)
					.then((r) => {
		          vm.loading = false;

		          if (!r) {
		            return;
		          }

		          vm.tasks = r;

		          if(!vm.tasks.length) {
		          	$rootScope.$broadcast('app.task.composer.toggle');
		          }

		          $rootScope.$safeApply();
					})
					.catch(onError);

			})
			.catch(onError);
	}

	function init() {
		if (!vm.isNew) {
			fetchData(projectId);
		} else {
			vm.loading = false;
		}
	}

	// Methods
	vm.deleteProject = deleteProject;

	// Subscribers
	subscribers.onTaskDeleted = $rootScope.$on('app.task.deleted', (e, task) => {
		if(!task) {
			return;
		}

		const index = vm.tasks.indexOf(task);
		vm.tasks.splice(index, 1);

		$rootScope.$broadcast('app.project.task.list.do.update', vm.tasks);

  	$rootScope.$safeApply();
  	return;
	});

	subscribers.onTaskUpdated = $rootScope.$on('app.task.updated', (e, task, isNew) => {
		if(!task || !task.id) {
			return;
		}

		TaskService
				.get(task.id, true)
				.then((r) => {
					if(r){
						let updatedTask = r;

						if(isNew) {
							vm.tasks.push(updatedTask);
						} else {
							angular.forEach(vm.tasks, (t) => {
								if(t.id === updatedTask.id){
									t = updatedTask;
								}
							});
						}

						$rootScope.$broadcast('app.project.task.list.do.update', vm.tasks);

						$rootScope.$safeApply();
					}
				});

  	return;
	});

	// Destructor
	$scope.$on('$destroy', () => {
	  // Remove subscribers
	  angular.forEach(subscribers, (fn) => {
	  	if(angular.isFunction(fn)){
	  		fn();
	  	}
	  });

	});

	init();

}

export default {
	name: 'ProjectCtrl',
	fn: ProjectCtrl
};
