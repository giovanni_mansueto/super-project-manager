function SignInCtrl($scope, $rootScope, SweetAlert, AuthService) {
  'ngInject';

  // ViewModel
  const vm = this;

  vm.user = {};

  function submit($form) {
    function onError() {
      SweetAlert.error('Invalid credentials');
    }

    if($form && $form.$valid) {
      AuthService
        .authenticate(vm.user)
        .then((r) => {
          if(r) {
            $rootScope.$broadcast('app.user.authenticated', r);
            
            setTimeout(() => {
              $rootScope.goToState('home');
            }, 100);

            return;
          }

          onError();

        })
        .catch(onError);
    }
    
  }
  
  // Methods
  vm.submit = submit;

}

export default {
  name: 'SignInCtrl',
  fn: SignInCtrl
};
