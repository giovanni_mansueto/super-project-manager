function SignUpCtrl($scope, $rootScope, SweetAlert, UserService, AuthService) {
  'ngInject';

  // ViewModel
  const vm = this;

  vm.user = {};

  function submit($form) {
    function onError(msg) {
      msg = angular.isString(msg) ? msg : 'Something is wrong, please, try again.'
      SweetAlert.error(msg);
    }

    let isValid = $form && $form.$valid;

    if(isValid && vm.user.alias.split(' ').length > 1) {
      onError('Invalid format. Username can not contains space.');

      return;
    } 

    if(isValid) {
      AuthService
        .signup(vm.user)
        .then((r) => {
          if(r) {
            $rootScope.$broadcast('app.user.authenticated', r);
            
            setTimeout(() => {
              $rootScope.goToState('home');
            }, 100);

            return;
          }

          onError(r);

        })
        .catch(onError);
    }

  }
  
  // Methods
  vm.submit = submit;

}

export default {
  name: 'SignUpCtrl',
  fn: SignUpCtrl
};
