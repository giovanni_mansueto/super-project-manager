function ProjectNameHolder(ProjectService) {
	'ngInject';

	return {
		restrict: 'EA',
		templateUrl: 'directives/project-name-holder.html',
		scope: {
			autofocus: '=',
			project: '='
		},
		link: (scope, element) => {

			let isSaving = false;
			let $input = element.find('input');

			scope.name = scope.project.name || '';

			function bindElements() {
				$input.on('change', () => {
					save();
				});

				$input.on('keyup', (e) => {
					if(e.which == 27){
			      e.target.blur();

			      return false;
			    }
				});

			}

			function update() {
				scope.project.name = scope.name;

				ProjectService
					.update(scope.project)
					.then(() => {
						isSaving = false;
					});

			}

			function create() {
				let project = {
					name: scope.name,
					userId: scope.$root.currentUser.id
				};

				ProjectService
					.create(project)
					.then((r) => {
						isSaving = false;
						if (r && r.id) {
							scope.$root.goToState('project', {
								id: r.id
							});
						}
					});

			}

			function save() {
				$input[0].blur();

				if (isSaving) {
					return;
				}

				isSaving = true;

				if (scope.project && scope.project.id) {
					update();
				} else {
					create();
				}

			}

			// Methods
			scope.save = save;

			// Subscribers
			scope.$watch('project', () => {
				scope.name = scope.project.name || '';
			});

			if(scope.autofocus) {
				setTimeout(() => {
					$input[0].focus();
				}, 100);
			}

			bindElements();

		}

	};
}

export default {
	name: 'appProjectNameHolder',
	fn: ProjectNameHolder
};
