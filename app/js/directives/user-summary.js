function UserSummary(AuthService) {
	'ngInject';

	return {
		restrict: 'EA',
		templateUrl: 'directives/user-summary.html',
		scope: {
			user: '='
		},
		link: (scope) => {

			scope.name = scope.user ? scope.user.name : '';

			function logout() {
				AuthService
					.logout()
					.then(() => {
						scope.$root.$broadcast('app.user.unauthenticated');
						scope.$root.goToState('signin');
					});
			}

			// Methods
			scope.logout = logout;

			scope.$watch('user', () => {
				scope.name = scope.user ? scope.user.name : '';
			});

		}

	};
}

export default {
	name: 'appUserSummary',
	fn: UserSummary
};
