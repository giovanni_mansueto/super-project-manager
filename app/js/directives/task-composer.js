function TaskComposer($timeout, SweetAlert, TaskService, UserService, TaskHelper) {
	'ngInject';

	return {
		restrict: 'EA',
		templateUrl: 'directives/task-composer.html',
		scope: {
			projectId: '=',
			disabled: '=?',
			initialOpen: '&open'
		},
		link: (scope, element) => {
			
			let descriptionEvaluated;
			let isSaving = false;
			let $input = element.find('input').eq(1);

			scope.open = scope.initialOpen();
			scope.error = null;
			scope.fullDescription = '';
			scope.userSearchTerm = '';
			scope.selectedUser = {};

			function toggleComposer() {
				scope.open = !scope.open;

				if(scope.open) {
					$timeout(() => {
						$input[0].focus();
					}, 100);
				}

			}

			function mapEvaluatedTaskDescription(evalObj) {
				if(!evalObj || !evalObj.user || !evalObj.user.id){
					return;
				}

				let task = TaskHelper.buildTask();

				task.projectId = scope.projectId;
				task.description = evalObj.description;
				task.dueDate = evalObj.dueDate.toISOString();
				task.userId = evalObj.user.id

				return task;
			}

			function create() {
				let editedTask = mapEvaluatedTaskDescription(descriptionEvaluated);
				
				if(editedTask){
					TaskService
						.create(editedTask)
						.then(() => {
							isSaving = false;
							
							scope.task = editedTask;
							scope.task.user = descriptionEvaluated.user;

							scope.fullDescription = '';

							scope.$root.$safeApply();

						});
				}else{
					SweetAlert.error('We can not save your task now', { title: 'Sorry :(' });
				}

			}

			function save() {
				if (isSaving || !scope.projectId) {
					return;
				}

				isSaving = true;

				UserService
					.getByAlias(descriptionEvaluated.userAlias)
					.then((r) => {
						isSaving = false;

						if(r && r.length){
							descriptionEvaluated.user = r[0];
							create();
							return;
						}

						scope.error = 'Invalid user';
						scope.$root.$safeApply();
					});

			}

			function bindElements() {

				$input.on('blur', () => {
					$timeout(() => {
						scope.userSearchTerm = '';
					}, 500);

					scope.$root.$safeApply();
				});

				$input.on('keyup', (e) => {
					let val = $input.val();

					scope.error = null;

					if(e.which == 27){
			      toggleComposer();

			      scope.$root.$safeApply();

			      return false;
			    }

					descriptionEvaluated = TaskHelper.evaluateDescription(val);

					if(descriptionEvaluated.fillPhase > 3) {
						e.preventDefault();
						scope.error = descriptionEvaluated.error;
							
						scope.$root.$safeApply();

						return false;
					}

					if (e.keyCode == 13) {
						if(descriptionEvaluated.isValid) {
							save();
						} else{
							scope.error = descriptionEvaluated.error;
							
							scope.$root.$safeApply();
						}

						return false;
					}

					// Check user owner
					if (descriptionEvaluated.userAlias && 
						!descriptionEvaluated.dueDate &&
						descriptionEvaluated.fillPhase === 2) {

						scope.userSearchTerm = descriptionEvaluated.userAlias;
			
					} else {
						scope.userSearchTerm = '';
					}

					scope.$root.$safeApply();

				});

			}

			scope.toggleComposer = toggleComposer;

			// Subscribers
			let subscriber = scope.$root.$on('app.task.composer.toggle', () => {
					toggleComposer();
			})

			scope.$watch('selectedUser', () => {
				if (!descriptionEvaluated || !scope.selectedUser || !scope.selectedUser.alias) {
					return;
				}

				descriptionEvaluated.userAlias = scope.selectedUser.alias;

				// scope.task.description = TaskHelper.toString(descriptionEvaluated);
				scope.fullDescription = TaskHelper.toString(descriptionEvaluated);

				$input[0].focus();

			});

			// Destructor
			scope.$on('$destroy', () => {
			  // Remove subscriber
			  subscriber();
			});

			bindElements();

		}

	};
}

export default {
	name: 'appTaskComposer',
	fn: TaskComposer
};
