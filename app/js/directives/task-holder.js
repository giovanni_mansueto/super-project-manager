function TaskHolder($timeout, SweetAlert, TaskService, UserService, TaskHelper) {
	'ngInject';

	return {
		restrict: 'EA',
		templateUrl: 'directives/task-holder.html',
		scope: {
			task: '='
		},
		link: (scope, element) => {

			let descriptionEvaluated;

			let isSaving = false;
			let lastEvaluatesUserAlias = scope.task.user.alias;
			let $input = element.find('input').eq(1);

			scope.error = null;
			scope.isEdit = false;
			scope.userSearchTerm = '';
			scope.selectedUser = scope.task.user;

			bindScopes();

			function bindScopes() {
				scope.fullDescription = TaskHelper.getFullDescription(scope.task);
				scope.dueDateText = TaskHelper.dateToString(scope.task.dueDate);
				scope.status = TaskHelper.getStatus(scope.task);

				scope.$root.$safeApply();

			}

			function editMode(edit) {
				scope.isEdit = edit;

				scope.$root.$safeApply();

				if (edit) {
					$timeout(() => {
						$input[0].focus();
					}, 85);
				}

			}

			function deleteTask() {
				function deleteAction() {
					TaskService
						.delete(scope.task.id)
						.then(() => {
							scope.$root.$broadcast('app.task.deleted', scope.task);
						});
				}

				SweetAlert.confirm('Are you sure?')
					.then((p) => {
						if (p) {
							deleteAction();
						}
					});

			}

			function mapEvaluatedTaskDescription(evaluateObj) {
				if(!evaluateObj || !evaluateObj.user || !evaluateObj.user.id){
					return;
				}

				let task = angular.copy(scope.task);

				task.description = evaluateObj.description;
				task.dueDate = evaluateObj.dueDate.toISOString();
				task.userId = evaluateObj.user.id

				return task;

			}

			function update() {
				let editedTask = mapEvaluatedTaskDescription(descriptionEvaluated);
				
				if(editedTask){
					TaskService
						.update(editedTask)
						.then(() => {
							angular.extend(scope.task, editedTask);
							scope.task.user = descriptionEvaluated.user;

							isSaving = false;

							scope.$root.$safeApply();

							bindScopes();
						});
				}else{
					SweetAlert.error('We can not save your task now', { title: 'Sorry :(' });
				}

			}

			function save() {
				$input[0].blur();

				if (isSaving) {
					return;
				}

				isSaving = true;
				editMode(false);

				UserService
					.getByAlias(descriptionEvaluated.userAlias)
					.then((r) => {
						isSaving = false;

						if(r && r.length){
							descriptionEvaluated.user = r[0];
							update();

							return;
						}

						editMode(true);

						scope.error = 'Invalid user';
						scope.$root.$safeApply();
					});

			}

			function completeTask() {
				let task = angular.copy(scope.task);

				TaskService
					.update(task);
			}

			function bindElements() {

				$input.on('blur', () => {
					$timeout(() => {
						scope.userSearchTerm = '';
					}, 500);

					scope.$root.$safeApply();
				});

				$input.on('keyup', (e) => {
					let val = $input.val();

					scope.error = null;

					if(e.which == 27){
			      editMode(false);

			      return false;
			    }

					descriptionEvaluated = TaskHelper.evaluateDescription(val);

					if (e.keyCode == 13) {
						if(descriptionEvaluated.isValid) {
							save();
						}else{
							scope.error = descriptionEvaluated.error;
							
							scope.$root.$safeApply();
						}

						return false;
					}

					if(descriptionEvaluated.fillPhase > 3) {
						e.preventDefault();
						scope.error = descriptionEvaluated.error;
							
						scope.$root.$safeApply();

						return false;
					}

					// Check user owner
					if (descriptionEvaluated.userAlias && 
						descriptionEvaluated.fillPhase >= 2 &&
						descriptionEvaluated.userAlias !== lastEvaluatesUserAlias) {

						lastEvaluatesUserAlias = descriptionEvaluated.userAlias;
						scope.userSearchTerm = descriptionEvaluated.userAlias;
			
					} else {
						scope.userSearchTerm = '';
					}

					scope.$root.$safeApply();

				});

			}


			// Methods
			scope.deleteTask = deleteTask;
			scope.editMode = editMode;
			scope.completeTask = completeTask;

			// Subscribers
			scope.$watch('task.isCompleted', () => {
				scope.status = TaskHelper.getStatus(scope.task);
			});

			scope.$watch('selectedUser', () => {
				if (!descriptionEvaluated || 
					!scope.selectedUser || 
					!scope.selectedUser.alias) {
					return;
				}

				descriptionEvaluated.userAlias = scope.selectedUser.alias;

				scope.fullDescription = TaskHelper.toString(descriptionEvaluated);

				$input[0].focus();

			});

			bindElements();

		}

	};
}

export default {
	name: 'appTaskHolder',
	fn: TaskHolder
};
