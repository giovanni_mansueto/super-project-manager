function ProjectSummary(TaskHelper) {
	'ngInject';

	return {
		restrict: 'EA',
		templateUrl: 'directives/project-summary.html',
		scope: {
			tasks: '=',
			selectedFilter: '='
		},
		link: (scope) => {

			const _GROUPS = ['late', 'completed'];

			scope.all = [];
			scope.late = [];
			scope.completed = [];

			function setFilter(value) {
				if(scope.selectedFilter === value) {
					return;
				}

				scope.selectedFilter = value;

				scope.$root.$safeApply();

			}

			function groupTasks(tasks) {
				let groups = {};

				angular.forEach(_GROUPS, (name) => {
					groups[name] = [];
				});

				angular.forEach(tasks, (task) => {
					let status = TaskHelper.getStatus(task);
					if(_GROUPS.indexOf(status) > -1) {
						groups[status].push(task);
					}
				});

				angular.forEach(groups, (group, key) => {
					scope[key] = group;
				});

				scope.$root.$safeApply();

			}

			// Subscribers
			let subscriber = scope.$root.$on('app.project.task.list.do.update', (e, tasks) => {
				groupTasks(tasks);
			});

			scope.$watch('tasks', (newVal) => {
				groupTasks(newVal);
			});

			// Methods
			scope.setFilter = setFilter;

			// Destructor
			scope.$on('$destroy', () => {
			  // Remove subscriber
			  subscriber();
			});

			groupTasks(scope.tasks);

		}

	};
}

export default {
	name: 'appProjectSummary',
	fn: ProjectSummary
};
