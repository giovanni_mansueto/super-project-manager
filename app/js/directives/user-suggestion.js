function UserSuggestion(UserService) {
	'ngInject';

	return {
		restrict: 'EA',
		templateUrl: 'directives/user-suggestion.html',
		scope: {
			searchTerm: '=',
			selectedUser: '=',
			suggestions: '=?',
			userProperty: '=?'
		},
		link: (scope) => {

			scope.userProperty = scope.userProperty || 'alias';

			scope.show = false;
			scope.suggestions = scope.suggestions || [];

			function fetchUsers(term) {
				return UserService
					.filterByAlias(term)
					.then((r) => {
						scope.suggestions = r || [];
						scope.show = !!scope.suggestions.length;

						scope.$root.$safeApply();
					});
			}

			function selectMatch(index) {
				if (scope.suggestions[index]) {
					scope.selectedUser = scope.suggestions[index];
					scope.show = false;
				}
			}

			scope.$watch('searchTerm', () => {
				if (scope.searchTerm) {
					fetchUsers(scope.searchTerm);
				} else {
					scope.show = false;
				}
			});

			scope.selectMatch = selectMatch;

		}

	};
}

export default {
	name: 'appUserSuggestion',
	fn: UserSuggestion
};
