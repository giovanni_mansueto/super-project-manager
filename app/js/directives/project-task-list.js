function ProjectTaskList(TaskHelper) {
	'ngInject';

	return {
		restrict: 'EA',
		templateUrl: 'directives/project-task-list.html',
		scope: {
			tasks: '=',
			selectedFilter: '='
		},
		link: (scope) => {

			scope.filteredTasks = [];

			function sortTasks(tasks) {
				scope.filteredTasks = tasks.sort((a, b) => {
					a = new Date(a.dueDate);
					b = new Date(b.dueDate);

				  return Date.compare(a, b);
				});

				scope.$root.$safeApply();
			}

			function filterTasks(tasks) {
				let filtered = [];

				if(!scope.selectedFilter || scope.selectedFilter === 'all'){
					filtered = tasks;
				} else {
					angular.forEach(tasks, (task) => {
						let status = TaskHelper.getStatus(task);
						if(status === scope.selectedFilter){
							filtered.push(task);
						}
					});
				}

				sortTasks(filtered);
			}

			// Subscribers
			let subscriber = scope.$root.$on('app.project.task.list.do.update', (e, tasks) => {
				filterTasks(tasks);
			});

			scope.$watch('tasks', () => {
				filterTasks(scope.tasks);
			});

			scope.$watch('selectedFilter', () => {
				filterTasks(scope.tasks);
			});

			// Destructor
			scope.$on('$destroy', () => {
			  // Remove subscriber
			  subscriber();
			});

			filterTasks(scope.tasks);

		}

	};
}

export default {
	name: 'appProjectTaskList',
	fn: ProjectTaskList
};
