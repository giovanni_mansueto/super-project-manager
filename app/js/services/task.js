function TaskService($rootScope, $http, AppSettings) {
	'ngInject';

	const apiUrl = AppSettings.serverApiHost + '/tasks';
	const service = {};

	function broadcastUpdate(data, isNew) {
		$rootScope.$broadcast('app.task.updated', data, isNew);
	}

	service.getAllByProject = (projectId, embed) => {
		let url = apiUrl + '?projectId=' + projectId;
		if (embed) {
			url += '&_expand=user';
		}

		return new Promise((resolve, reject) => {
			$http.get(url).success((data) => {
				resolve(data);
			}).error((err, status) => {
				reject(err, status);
			});
		});
	};

	service.get = (id, embed) => {
		let url = apiUrl + '/' + id;
		if (embed) {
			url += '?_expand=user';
		}

		return new Promise((resolve, reject) => {
			$http.get(url).success((data) => {
				resolve(data);
			}).error((err, status) => {
				reject(err, status);
			});
		});
	};

	service.create = (task) => {
		let url = apiUrl;

		if (task.user) {
			delete task.user;
		}

		return new Promise((resolve, reject) => {
			$http.post(url, task).success((data) => {
				broadcastUpdate(data, true);
				resolve(data);
			}).error((err, status) => {
				reject(err, status);
			});
		});
	};

	service.update = (task, notBrodcast) => {
		let url = apiUrl + '/' + task.id;

		if (task.user) {
			delete task.user;
		}

		return new Promise((resolve, reject) => {
			$http.put(url, task).success((data) => {
				if(!notBrodcast) {
					broadcastUpdate(data);
				}
				
				resolve(data);
			}).error((err, status) => {
				reject(err, status);
			});
		});
	};

	service.delete = (id) => {
		let url = apiUrl + '/' + id;

		return new Promise((resolve, reject) => {
			$http.delete(url).success((data) => {
				resolve(data);
			}).error((err, status) => {
				reject(err, status);
			});
		});
	};

	return service;

}

export default {
	name: 'TaskService',
	fn: TaskService
};
