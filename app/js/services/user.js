function UserService($rootScope, $http, AppSettings) {
	'ngInject';

	const apiUrl = AppSettings.serverApiHost + '/users';
	const service = {};

	service.filterByAlias = (alias) => {
		let url = apiUrl + '?alias_like=' + alias;

		return new Promise((resolve, reject) => {
			$http.get(url).success((data) => {
				resolve(data);
			}).error((err, status) => {
				reject(err, status);
			});
		});
	};

	service.getByAlias = (alias) => {
		let url = apiUrl + '?alias=' + alias;

		return new Promise((resolve, reject) => {
			$http.get(url).success((data) => {
				resolve(data);
			}).error((err, status) => {
				reject(err, status);
			});
		});
	};

	return service;

}

export default {
	name: 'UserService',
	fn: UserService
};
