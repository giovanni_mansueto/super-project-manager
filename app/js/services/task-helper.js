function TaskHelper($filter) {
	'ngInject';

	const _STATUS = {
		inTime: 'in-time',
		late: 'late',
		completed: 'completed'
	};

	const _ERROR = {
		description: 'Type some description, owner and due date',
		user: 'Type a valid user',
		dueDate: 'Type a valid due date format: "M/d", "today", "tomorrow"',
		format: 'Invalid format. Type "description, @owner, due date"'
	};

	const _ALLOWED_DATE_FORMAT = {
		yesterday: Date.parse('yesterday'),
		today: Date.parse('today'),
		tomorrow: Date.parse('tomorrow')
	};

	const service = {};

	const evaluator = {};

	let today = Date.today();

	function trim(value) {
		return $filter('trim')(value);
	}

	function dateToString(date) {
		let text = '';

		date = Date.parse(date);

		angular.forEach(_ALLOWED_DATE_FORMAT, function(value, key) {
		  if(Date.equals(value, date)){
		  	text = key;
		  }
		});

		if(!text){
			text = date.toString('M/d');
		}

		return text;
	}

	function toString(description, userAlias, dueDate) {
		let fullDescription = '';

		if (description) {
			fullDescription += description + ', ';
		}

		if (userAlias) {
			fullDescription += '@' + userAlias + ', ';
		}

		if (dueDate) {
			fullDescription += dateToString(dueDate);
		}

		return fullDescription;
		
	}

	evaluator.user = (user) => {
		user = trim(user);

		let userMarker = user.split('@');
		if (userMarker.length > 1) {
			userMarker = userMarker[1].split(' ');
			user = userMarker[0];
		} else {
			user = null;
		}

		return user;

	};

	evaluator.dueDate = (dueDate) => {
		let dueDateText = trim(dueDate);

		if(_ALLOWED_DATE_FORMAT[dueDateText]){
			dueDate = _ALLOWED_DATE_FORMAT[dueDateText];

			return dueDate;
		}

		let parts = dueDateText.split('/');

		if(parts.length !== 2){
			return null;
		}

		dueDate = Date.parse(dueDateText);

		return dueDate;

	};

	service.getStatus = (task) => {
		if (task.isCompleted) {
			return _STATUS.completed;
		}

		if (today > new Date(task.dueDate)) {
			return _STATUS.late;
		}

		return _STATUS.inTime;

	};

	service.evaluateDescription = (fullDescription) => {
		let parts = (fullDescription || '').split(',');

		let description = parts[0];
		let user = parts[1];
		let dueDateText = parts[2];
		let fillPhase = parts.length;
		let dueDate;
		let error;

		if (description) {
			description = trim(description);
		}else{
			error =  _ERROR.description;
		}

		if(fillPhase > 3) {
			error = _ERROR.format;
		}

		if (user) {
			user = evaluator.user(user);
		}

		if(!user){
			error =  error || _ERROR.user;
		}

		if (dueDateText) {
			dueDate = evaluator.dueDate(dueDateText);
		}

		if(!dueDate){
			error =  error || _ERROR.dueDate;
		}

		return {
			isValid: !error && fillPhase === 3,
			description: description,
			userAlias: user,
			dueDate: dueDate,
			fillPhase: fillPhase,
			error: error
		};

	};

	service.toString = (evalObj) => {
		return toString(evalObj.description, evalObj.userAlias, evalObj.dueDate);
	};

	service.getFullDescription = (task) => {
		return toString(task.description, task.user.alias, task.dueDate);
	};

	service.buildTask = () => {
		return {
      projectId: 0,
      description: '',
      userId: 0,
      dueDate: null,
      isCompleted: false
    }
	};

	service.dateToString = dateToString;

	return service;

}

export default {
	name: 'TaskHelper',
	fn: TaskHelper
};
