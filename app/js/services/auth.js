function AuthService($rootScope, $http, AppSettings, UserService, localStorageService, Encoder) {
	'ngInject';

	const apiUrl = AppSettings.serverApiHost + '/users';
	const service = {};

	const userStorage = {};

	userStorage.set = (value) => {
		if(value && value.password) {
			value.password = null
		}

		localStorageService.set('loggedUser', value);

		return value;
	};

	userStorage.get = () => {
		return localStorageService.get('loggedUser');
	};

	service.authenticate = (user) => {
		user = angular.copy(user);

		user.password = Encoder.base64.encode(user.password) || '';
		
		let url = apiUrl + `?alias=${user.alias}&password=${user.password}`;

		return new Promise((resolve, reject) => {
			$http.get(url).success((data) => {
				if(data && data.length) {
					resolve(userStorage.set(data[0]));

					return;
				}

				reject();
			}).error((err, status) => {
				reject(err, status);
			});
		});

	};

	service.signup = (user) => {
		let url = apiUrl;

		user = angular.copy(user);

		user.password = Encoder.base64.encode(user.password) || '';
		
		return new Promise((resolve, reject) => {
			let getUrl = apiUrl + `?alias=${user.alias}`;

			$http.get(getUrl).success((data) => {
				if(data && data.length) {
					reject('Username already in use');

					return;
				}

				$http.post(url, user).success((data) => {
					data.password = null;
					resolve(userStorage.set(data));
				}).error((err, status) => {
					reject(err, status);
				});

			}).error((err, status) => {
				reject(err, status);
			});

		});
	};

	service.logout = () => {
		return new Promise((resolve) => {
			userStorage.set(null);
			resolve();
		});

	};

	service.getCurrent = () => {
		return userStorage.get();
	};

	service.isAuthenticated = () => {
		return !!service.getCurrent();
	}

	return service;

}

export default {
	name: 'AuthService',
	fn: AuthService
};
