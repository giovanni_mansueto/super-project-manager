function ProjectService($rootScope, $http, AppSettings) {
	'ngInject';

	const apiUrl = AppSettings.serverApiHost + '/projects';
	const service = {};

	function broadcastUpdate(data) {
		$rootScope.$broadcast('app.project.updated', data);
	}

	function broadcastDelete() {
		$rootScope.$broadcast('app.project.deleted');
	}

	service.getAll = (userId, embed) => {
		let url = apiUrl + '?userId=' + userId;
		if (embed) {
			url += '&_embed=tasks';
		}

		return new Promise((resolve, reject) => {
      if(!userId) {
        reject();
      }

			$http.get(url).success((data) => {
				resolve(data);
			}).error((err, status) => {
				reject(err, status);
			});
		});
	};

	service.get = (userId, id, embed) => {
    let url = apiUrl + '?id=' + id + '&userId=' + userId;
		if (embed) {
			url += '?_embed=tasks';
		}

		return new Promise((resolve, reject) => {
			$http.get(url).success((data) => {
        if(data && data.length > 0) {
          resolve(data[0]);
        }
        
				reject();
			}).error((err, status) => {
				reject(err, status);
			});
		});
	};

	service.create = (project) => {
		let url = apiUrl;

		if (project.tasks) {
			delete project.tasks;
		}

		return new Promise((resolve, reject) => {
			$http.post(url, project).success((data) => {
				broadcastUpdate(data);
				resolve(data);
			}).error((err, status) => {
				reject(err, status);
			});
		});
	};

	service.update = (project) => {
		let url = apiUrl + '/' + project.id;

		if (project.tasks) {
			delete project.tasks;
		}

		return new Promise((resolve, reject) => {
			$http.put(url, project).success((data) => {
				broadcastUpdate(data);
				resolve(data);
			}).error((err, status) => {
				reject(err, status);
			});
		});
	};

	service.delete = (id) => {
		let url = apiUrl + '/' + id + '?_embed=tasks';

		return new Promise((resolve, reject) => {
			$http.delete(url).success((data) => {
				broadcastDelete();
				resolve(data);
			}).error((err, status) => {
				reject(err, status);
			});
		});
	};

	return service;

}

export default {
	name: 'ProjectService',
	fn: ProjectService
};
