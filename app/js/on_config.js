function OnConfig($stateProvider, $locationProvider, $urlRouterProvider, $compileProvider, localStorageServiceProvider) {
	'ngInject';

	localStorageServiceProvider.setPrefix('app');

	if (process.env.NODE_ENV === 'production') {
		$compileProvider.debugInfoEnabled(false);
	}

	$locationProvider.html5Mode({
		enabled: false,
		requireBase: false
	});

	function skipIfAuthenticated($q, $state, AuthService) {
	  let defer = $q.defer();
	  if(AuthService.isAuthenticated()) {
	    defer.reject();
	  } else {
	    defer.resolve();
	  }

	  return defer.promise;
	}

	function redirectIfNotAuthenticated($q, $state, $timeout, AuthService) {
	  let defer = $q.defer();
	  if(AuthService.isAuthenticated()) {
	    defer.resolve();
	  } else {
	    $timeout(function () {
	      $state.go('signin');
	    });
	    defer.reject();
	  }

	  return defer.promise;
	}

	$stateProvider
		.state('home', {
			url: '/',
			controller: 'HomeCtrl',
			templateUrl: 'home.html',
			title: ''
		})
		.state('signin', {
			url: '/signin',
			controller: 'SignInCtrl as vm',
			templateUrl: 'signin.html',
			title: 'Sign In',
			bodyClass: 'signin-page',
			resolve: {
        skipIfAuthenticated: skipIfAuthenticated
      }
		})
		.state('signup', {
			url: '/signup',
			controller: 'SignUpCtrl as vm',
			templateUrl: 'signup.html',
			title: 'Sign up',
			bodyClass: 'signin-page signup',
			resolve: {
        skipIfAuthenticated: skipIfAuthenticated
      }
		})
		.state('new-project', {
			url: '/new-project',
			controller: 'ProjectCtrl as vm',
			templateUrl: 'project.html',
			title: 'New Project',
			resolve: {
				redirectIfNotAuthenticated: redirectIfNotAuthenticated,
				projectId: function() {
					return null;
				}
			}
		})
		.state('project', {
			url: '/project/{id}',
			controller: 'ProjectCtrl as vm',
			templateUrl: 'project.html',
			title: 'Project',
			resolve: {
				redirectIfNotAuthenticated: redirectIfNotAuthenticated,
				projectId: function($stateParams) {
					return $stateParams.id;
				}
			}
		});

	$urlRouterProvider.otherwise('/signin');

}

export default OnConfig;
